package it.cordand.storage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.cordand.dataset.Dataset;
import it.cordand.ml.analyzer.PerformanceAnalysis;
import it.cordand.ml.model.RuleConjunction;
import it.cordand.ml.prism.Prism;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class FileManager {
    private static final String OUTPUT_PATH = "output";
    private static File outputDir = null;
    public static String writeRulesToFile(Prism prism){
        File outputDir = getOutputDir();
        File outputFile = new File(String.format("%s/ -d %s -i %d.json",outputDir.getPath(),prism.getDatasetName(),prism.getDatasetSize()));

        String jsonEncodedString = jsonEncodeRules(prism);

        try {
            Files.write(Paths.get(outputFile.getPath()),jsonEncodedString.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFile.getPath();
    }

    public static String writeReportToFile(PerformanceAnalysis analysis){
        File outputDir = getOutputDir();
        File outputFile = new File(String.format("%s/PerformanceReport.txt",outputDir.getPath()));

        String content = analysis.toString();

        try {
            Files.write(Paths.get(outputFile.getPath()),content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFile.getPath();
    }


    private static File getOutputDir() {
        if(outputDir != null)
            return outputDir;
        File outputPath = new File(OUTPUT_PATH);
        if(!outputPath.exists())
            outputPath.mkdir();
        int fileCount = outputPath.listFiles().length;
        File outputDir = new File(String.format("%s/ rules -n %d",outputPath,fileCount));
        if(!outputDir.exists())
            outputDir.mkdir();
        FileManager.outputDir = outputDir;
        return outputDir;
    }

    private static String jsonEncodeRules(Prism prism) {
        ArrayList<RuleConjunction> rulesSet = prism.getRulesSet();
        String json = new Gson().toJson(rulesSet);
        return json;
    }

    public static ArrayList<RuleConjunction> readRulesFromFile(String filePath) throws IOException {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(contentBuilder::append);
        }
        String jsonContent = contentBuilder.toString();
        ArrayList<RuleConjunction> ruleConjunctions = new Gson().fromJson(jsonContent,new TypeToken<ArrayList<RuleConjunction>>(){}.getType());
        return ruleConjunctions;
    }

    public static String getDatabaseName(String outputFile) {
        String folder = outputFile.substring(outputFile.lastIndexOf("/"));

        return folder.substring(folder.indexOf("-d "),folder.indexOf(" -i "));
    }
}

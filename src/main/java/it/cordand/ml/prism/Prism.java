package it.cordand.ml.prism;

import it.cordand.dataset.Dataset;
import it.cordand.dataset.Instance;
import it.cordand.ml.model.Rule;
import it.cordand.ml.model.RuleConjunction;
import it.cordand.utils.CollectionUtils;
import it.cordand.utils.FeatureValue;

import java.io.IOException;
import java.util.*;

public class Prism {

    private Dataset dataset;

    private ArrayList<RuleConjunction> rulesSet = new ArrayList<>();

    public Prism(Dataset dataset) {
        this.dataset = dataset;
    }

    private long generationTime = 0;

    public void generateRulesSet(){
        generationTime = System.currentTimeMillis();
        for (String classValue : dataset.getClassValues()) {
            ArrayList<Instance> instancesWithClass = (ArrayList<Instance>) dataset.getInstancesWithClass(classValue).clone();
            while(instancesWithClass.size() > 0) {
                RuleConjunction bestRuleForClassValue = findBestRulesForClassValue(classValue, instancesWithClass);
                removeInstancesSatisfyingRule(bestRuleForClassValue,instancesWithClass);
                rulesSet.add(bestRuleForClassValue);
            }
        }
        generationTime = System.currentTimeMillis() - generationTime;
    }

    private void removeInstancesSatisfyingRule(RuleConjunction bestRuleForClassValue, ArrayList<Instance> instancesWithClass) {

        instancesWithClass.removeAll(bestRuleForClassValue.evaluate(instancesWithClass));

    }

    private RuleConjunction findBestRulesForClassValue(String classValue, ArrayList<Instance> instancesWithClass) {
        RuleConjunction ruleConjunction = new RuleConjunction(classValue);
        double precision = 0;
        do {
            Rule bestRule = findBestRule(classValue, instancesWithClass, ruleConjunction);
            if(bestRule == null)
                break;
            ruleConjunction.addRule(bestRule);
            precision = ruleConjunction.evaluatePrecision(dataset);
        }while(precision < 1);
        return ruleConjunction;

    }

    private Rule findBestRule(String classValue, ArrayList<Instance> instancesWithClass,
                              RuleConjunction ruleConjunction) {
        HashMap<Integer,TreeMap<String,Integer>> featureValueCount = new HashMap<>();
        ArrayList<Instance> instancesToConsider = ruleConjunction.evaluate(instancesWithClass);

        for (Instance instance : instancesToConsider) {
            for (int i = 0; i<instance.size()-1 /* Remove class from instance list */;i++) {
                TreeMap<String, Integer> integerHashMap = featureValueCount.get(i);
                if(integerHashMap == null) {
                    integerHashMap = new TreeMap<>();
                    featureValueCount.put(i,integerHashMap);
                }
                Integer integer = integerHashMap.get(instance.getValue(i));
                if(integer == null)
                    integerHashMap.put(instance.getValue(i),1);
                else
                    integerHashMap.put(instance.getValue(i),integerHashMap.remove(instance.getValue(i))+1);
            }
        }

        HashMap<FeatureValue,Double> featureImportance = new HashMap<>();
        featureValueCount.forEach((key,value)->{
            List<Map.Entry<String, Integer>> entries = CollectionUtils.entriesSortedByValues(value, CollectionUtils.Order.DESC);
            int instancesCount = 0;

            Map.Entry<String, Integer> lastEntry = entries.get(0);
            String featureValue = lastEntry.getKey();
            try {
                instancesCount = dataset.countInstancesWithFeatureValue(key, featureValue);
            } catch (IOException e) {
                e.printStackTrace();
            }
            featureImportance.put(new FeatureValue(key, featureValue,instancesCount),(double)lastEntry.getValue()/instancesCount);
        });

        List<Map.Entry<FeatureValue, Double>> featuresOrdered = CollectionUtils.entriesSortedByValues(featureImportance, CollectionUtils.Order.DESC);

        Rule toReturn = null;
        int count = 0;
            do{
                double maxPrecision = featuresOrdered.get(count).getValue();
                int maxItemCount = 0;
                for (Map.Entry<FeatureValue, Double> entry :
                        featuresOrdered) {
                    int totalItemCount = entry.getKey().getTotalItemCount();
                    double precision = entry.getValue();
                    if (totalItemCount > maxItemCount && precision >= maxPrecision) {
                        maxItemCount = totalItemCount;
                        toReturn = new Rule(featuresOrdered.get(count).getKey(), Rule.RuleCondition.EQUAL, classValue);
                    }
                }
                count++;
            }while ((ruleConjunction.contains(toReturn)||!ruleConjunction.isFeatureFree(toReturn.getFeatureIndex())) && count < featuresOrdered.size());
        return toReturn;
    }

    public ArrayList<RuleConjunction> getRulesSet() {
        return rulesSet;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public Object getDatasetName() {
        return dataset.getDatasetName();
    }

    public int getDatasetSize() {
        return dataset.getSize();
    }

    public long getGenerationTime() {
        return generationTime;
    }
}

package it.cordand.ml.analyzer;

import it.cordand.ml.model.RuleConjunction;

public class RuleEvaluation {
    RuleConjunction rules;
    private double coverage,precision;

    public RuleEvaluation(RuleConjunction rules, double coverage, double precision) {
        this.rules = rules;
        this.coverage = coverage;
        this.precision = precision;
    }

    @Override
    public String toString() {
        return rules + " \n\t coverage:" + coverage +
                ", precision:" + precision;
    }
}

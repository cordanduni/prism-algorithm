package it.cordand.ml.analyzer;

import it.cordand.dataset.Dataset;
import it.cordand.dataset.Instance;
import it.cordand.ml.model.Rule;
import it.cordand.ml.model.RuleConjunction;
import it.cordand.ml.prism.Prism;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class PerformanceAnalyzer {

    Prism prismInstance;

    public PerformanceAnalyzer(Prism prismInstance) {
        this.prismInstance = prismInstance;
    }

    /**
     * Warning: This function not only tests the rules, but also removes from the testDataset the classified instances
     * @param testDataset
     * @param testDataset
     */
    public PerformanceAnalysis testAgainstDataset(Dataset testDataset) {
        PerformanceAnalysis performanceAnalysis = new PerformanceAnalysis();
        performanceAnalysis.setGenerationTime(prismInstance.getGenerationTime());
        performanceAnalysis.setDatasetSize(prismInstance.getDatasetSize());
        performanceAnalysis.setTestDatasetSize(testDataset.getSize());

        int correctlyClassifiedInstances = 0;
        int incorrectlyClassifiedInstances = 0;
        for (RuleConjunction rules : prismInstance.getRulesSet()) {
            try {
                int truePositives = 0,falsePositives = 0;
                ArrayList<Instance> instances = testDataset.queryInstances(rules, true);
                for (Instance i :
                        instances) {
                    if(i.getClassValue().equals(rules.getClassValue())){
                        truePositives++;
                        correctlyClassifiedInstances += 1;
                    }else{
                        falsePositives++;
                        incorrectlyClassifiedInstances += 1;
                    }
                }
                int totalNumberOfInstancesWithClass = testDataset.countInstancesForClass(rules.getClassValue());
//                int totalNumberOfInstancesWithClass = dataset.countInstancesWithFeatureValue(dataset.getClassIndex(),rule.getClassValue());
                double precision = (double)truePositives/(truePositives+falsePositives);
                double coverage = (double)truePositives/totalNumberOfInstancesWithClass;

                System.out.println("Rule: " + rules + "\n\tPrecision: " + precision + " Coverage: " + coverage);
                performanceAnalysis.addEvaluation(new RuleEvaluation(rules,coverage,precision));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        performanceAnalysis.setNotClassifiedInstances(testDataset.getSize());
        performanceAnalysis.setCorrectlyClassifiedInstances(correctlyClassifiedInstances);
        performanceAnalysis.setNotCorrectlyClassifiedInstances(incorrectlyClassifiedInstances);

        System.out.println("Not classified instances: " + testDataset.getSize());
        System.out.println("Correctly classified instances: " + correctlyClassifiedInstances);
        System.out.println("Not correctly classified instances: " + incorrectlyClassifiedInstances);

        return performanceAnalysis;
    }


}

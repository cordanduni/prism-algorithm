package it.cordand.ml.analyzer;

import java.util.ArrayList;

public class PerformanceAnalysis {
    private long correctlyClassifiedInstances,incorrectlyClassifiedInstances,notClassifiedInstances;

    private ArrayList<RuleEvaluation> ruleEvaluations = new ArrayList<>();
    private long generationTime;
    private long datasetSize;
    private int testDatasetSize;


    public void setNotClassifiedInstances(int notClassifiedInstances) {
        this.notClassifiedInstances = notClassifiedInstances;
    }

    public void setCorrectlyClassifiedInstances(int correctlyClassifiedInstances) {
        this.correctlyClassifiedInstances = correctlyClassifiedInstances;
    }

    public void setNotCorrectlyClassifiedInstances(int notCorrectlyClassifiedInstances) {
        this.incorrectlyClassifiedInstances = notCorrectlyClassifiedInstances;
    }

    public void addEvaluation(RuleEvaluation ruleEvaluation) {
        ruleEvaluations.add(ruleEvaluation);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("******* Rules Generation ********\n");
        builder.append("* Dataset Size: ").append(datasetSize).append(" Instances\n");
        builder.append("* Generation Time: ").append(String.format("%.4f",((double)generationTime/1000))).append(" s\n\n");
        builder.append("******* Testing ********\n");
        builder.append("* Test Dataset Size: ").append(testDatasetSize).append(" Instances\n");
        builder.append("******* Rules Evaluation Result ********\n");
        builder.append("* Correctly classified instances: " + correctlyClassifiedInstances).append("\n");
        builder.append("* Incorrectly classified instances: " + incorrectlyClassifiedInstances).append("\n");
        builder.append("* Not classified instances: " + notClassifiedInstances).append("\n");
        builder.append("\n");

        for (RuleEvaluation ruleEvaluation :
                ruleEvaluations) {
            builder.append(ruleEvaluation.toString()).append("\n");
        }
        return builder.toString();
    }

    public void setGenerationTime(long generationTime) {
        this.generationTime = generationTime;
    }

    public void setDatasetSize(int datasetSize) {
        this.datasetSize = datasetSize;
    }

    public void setTestDatasetSize(int testDatasetSize) {
        this.testDatasetSize = testDatasetSize;
    }
}

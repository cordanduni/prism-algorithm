package it.cordand.ml.model;

import it.cordand.dataset.Instance;
import it.cordand.utils.FeatureValue;

public class Rule implements Comparable<Rule>{

    private int featureIndex;
    private String featureValue;
    private String classValue;
    private RuleCondition ruleCondition;


    public Rule(FeatureValue featureValuePairDoubleEntry, RuleCondition ruleCondition, String classValue) {
        this.featureValue = featureValuePairDoubleEntry.getValue();
        this.featureIndex = featureValuePairDoubleEntry.getFeatureIndex();
        this.ruleCondition = ruleCondition;
        this.classValue = classValue;
    }


    public Rule(int featureIndex, String featureValue, RuleCondition ruleCondition, String classValue) {
        this.featureIndex = featureIndex;
        this.featureValue = featureValue;
        this.ruleCondition = ruleCondition;
        this.classValue = classValue;
    }


    public int getFeatureIndex() {
        return featureIndex;
    }

    public String getFeatureValue() {
        return featureValue;
    }

    public String getClassValue() {
        return classValue;
    }

    public boolean isClassCorrect(Instance instance) {
        return classValue.equals(instance.getClassValue());
    }

    @Override
    public int compareTo(Rule o) {
        if(o.classValue.equals(classValue) &&
                o.featureValue.equals(featureValue) &&
                o.featureIndex == featureIndex)
            return 0;
        else
            return o.featureIndex != featureIndex ? Integer.compare(o.featureIndex, featureIndex) : o.featureValue.compareTo(featureValue);
    }

    public enum RuleCondition{
        EQUAL,NOT_EQUAL,/* UNUSED FOR NOW*/GREATER, GREATER_OR_EQUAL,LESS,LESS_OR_EQUAL
    }

    public boolean evaluateRule(Instance instance){
        return evaluateRule(instance.getValue(featureIndex));
    }

    private boolean evaluateRule(String value){
        switch (ruleCondition){
            case EQUAL:
                return featureValue.equals(value);
            case NOT_EQUAL:
                return !featureValue.equals(value);
        }
        return false;
    }

    @Override
    public String toString() {
        return "When" +
                " featureIndex = " + featureIndex +
                " AND featureValue = " + featureValue +
                " ==> Class '" + ruleCondition +" " + classValue;
    }
}

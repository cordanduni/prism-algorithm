package it.cordand.ml.model;

import it.cordand.dataset.Dataset;
import it.cordand.dataset.Instance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

public class RuleConjunction {
    private TreeSet<Rule> rules = new TreeSet<>();
    private String classValue;
    private TreeSet<Integer> usedFeatures = new TreeSet<>();

    public RuleConjunction(String classValue) {

        this.classValue = classValue;
    }

    public boolean isFeatureFree(int featureId){
        return !usedFeatures.contains(featureId);
    }

    public void addRule(Rule rule){
        rules.add(rule);
        usedFeatures.add(rule.getFeatureIndex());
    }

    public ArrayList<Instance> evaluate(ArrayList<Instance> instances){
        ArrayList<Instance> instancesRespectingRule = new ArrayList<>();
        getInstancesRespectingRule(instances, instancesRespectingRule);

        return instancesRespectingRule;
    }

    private void getInstancesRespectingRule(ArrayList<Instance> instances, ArrayList<Instance> instancesRespectingRule) {
        for (Instance instance : instances) {
            boolean shouldAdd = true;
            for (Rule rule :
                    rules) {
                if (!rule.evaluateRule(instance)) {
                    shouldAdd = false;
                    break;
                }
            }
            if(shouldAdd)
                instancesRespectingRule.add(instance);
        }
    }

    public double evaluatePrecision(Dataset dataset){
        ArrayList<Instance> instancesRespectingRule = null;
        try {
            instancesRespectingRule = dataset.queryInstances(this, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Instance> instancesCorrectClass = new ArrayList<>();
        for (Instance instance : instancesRespectingRule) {
            boolean shouldAdd = true;
            for (Rule rule : rules) {
                if (!rule.isClassCorrect(instance)) {
                    shouldAdd = false;
                    break;
                }
            }
            if(shouldAdd)
                instancesCorrectClass.add(instance);
        }


        return (double)instancesCorrectClass.size()/instancesRespectingRule.size();
    }

    public TreeSet<Rule> getRules() {
        return rules;
    }

    public String getClassValue() {
        return classValue;
    }

    @Override
    public String toString() {
        String s = "";
        for (Rule r :
                rules) {
            s += r + " AND ";
        }
        s = s.substring(0,s.length() - 5);
        return "" + s;
    }

    public int rulesSetSize() {
        return rules.size();
    }

    public Rule getRule() {
        return rules.first();
    }

    public boolean contains(Rule toReturn) {
        return rules.contains(toReturn);
    }
}

package it.cordand;

import it.cordand.dataset.Dataset;
import it.cordand.io.CsvReader;
import it.cordand.ml.analyzer.PerformanceAnalysis;
import it.cordand.ml.analyzer.PerformanceAnalyzer;
import it.cordand.ml.prism.Prism;
import it.cordand.storage.FileManager;

import java.io.IOException;

public class App 
{
    public static void main( String[] args ) throws IOException {

        if(args.length != 6 || !args[0].equals("-d") || !args[2].equals("-n") || (!args[4].equals("-t") && !args[4].equals("-td"))) {
            System.out.println("Error --");
            System.out.println("Usage java -jar prism.jar -d [csv dataset path] -n [datasetName] [-t [percentage to be used for test] | -td [test_dataset_path]]");
            System.exit(0);
        }
        /** Phase 1*/
        Dataset[] testDataset = null;
        boolean usingTestDataset = false;
        if(args[4].equals("-td")){
            usingTestDataset = true;
            testDataset = CsvReader.readCSVContent(args[5],args[3] +"_TEST",0);
        }
        Dataset[] dataset;

        if(!usingTestDataset)
            dataset = CsvReader.readCSVContent(args[1],args[3],Integer.parseInt(args[5]));
        else
            dataset = CsvReader.readCSVContent(args[1],args[3],0);

        /** Phase 2*/
        Prism prism = new Prism(dataset[0]);
        prism.generateRulesSet();

        /** Phase 3*/
        PerformanceAnalyzer performanceAnalyzer = new PerformanceAnalyzer(prism);
        PerformanceAnalysis performanceAnalysis = performanceAnalyzer.testAgainstDataset(usingTestDataset ? testDataset[0] : dataset[1]);

        FileManager.writeReportToFile(performanceAnalysis);
        FileManager.writeRulesToFile(prism);


    }
}

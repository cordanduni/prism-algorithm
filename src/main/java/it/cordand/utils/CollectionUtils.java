package it.cordand.utils;

import java.util.*;

import static it.cordand.utils.CollectionUtils.Order.ASC;

public class CollectionUtils {

    public enum Order{
        ASC,DESC
    }

    public static <K,V extends Comparable<? super V>> List<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map, Order order) {

        List<Map.Entry<K,V>> sortedEntries = new ArrayList<Map.Entry<K,V>>(map.entrySet());

        Collections.sort(sortedEntries, (e1, e2) -> {
                    if(order == ASC)
                        return -e2.getValue().compareTo(e1.getValue());
                    else
                        return e2.getValue().compareTo(e1.getValue());

                }
        );

        return sortedEntries;
    }
}

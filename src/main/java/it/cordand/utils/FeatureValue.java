package it.cordand.utils;

public class FeatureValue implements Comparable<FeatureValue>{
    private String value;
    private int featureIndex;
    private int totalNumberOfInstancesWithFV = 0;

    public FeatureValue(int featureIndex, String featureValue, int totalNumberOfInstancesWithFV) {

        this.featureIndex = featureIndex;
        value = featureValue;
        this.totalNumberOfInstancesWithFV = totalNumberOfInstancesWithFV;
    }

    public String getValue() {
        return value;
    }

    public int getFeatureIndex() {
        return featureIndex;
    }

    @Override
    public int compareTo(FeatureValue o) {
        return getValue().compareTo(o.getValue());
    }

    public int getTotalItemCount() {
        return totalNumberOfInstancesWithFV;
    }
}

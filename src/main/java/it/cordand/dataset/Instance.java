package it.cordand.dataset;

import org.apache.commons.csv.CSVRecord;

import java.util.HashMap;

public class Instance implements Comparable<Instance>{

    //Values will contain the feature-value pairs for the instance
    private HashMap<Integer,String> values;


    public Instance() {
        values = new HashMap<>();
    }

    public Instance(CSVRecord record) {
        values = new HashMap<>();
        for (int i = 0; i < record.size(); i++) {
            addValue(i,record.get(i));
        }
    }

    public void addValue(int featureIndex,String value){
        values.put(featureIndex,value);
    }

    public String getValue(int feature) {
        return values.get(feature);
    }

    public int size() {
        return values.size();
    }

    public String getClassValue() {
        return values.get(values.size() - 1);
    }

    @Override
    public int compareTo(Instance o) {
        for(Integer i : values.keySet()){
            if(!o.getValue(i).equals(values.get(i)))
                return -1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        StringBuilder valuesStringBuilder = new StringBuilder();
        values.forEach((i,s)->valuesStringBuilder.append(s));
        return valuesStringBuilder.toString().hashCode();
    }

    public String stringValue() {
        StringBuilder valuesStringBuilder = new StringBuilder();
        values.forEach((i,s)->valuesStringBuilder.append(s));
        return valuesStringBuilder.toString();
    }
}

package it.cordand.dataset;

import it.cordand.dataset.helper.TypeHelper;
import it.cordand.ml.model.Rule;
import it.cordand.ml.model.RuleConjunction;
import it.cordand.utils.CollectionUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class Dataset {

    private final String datasetName;
    private Map<String,Integer> headerMap;
    private Map<Integer,String> invertedHeaderMap;

    private HashMap<String,Type> featureTypeMap = new HashMap<>();
    private HashMap<String,ArrayList<Instance>> classInstancesMap = new HashMap<>();
    private int classFeatureIndex;

    private TreeMap<String,Integer> classesCount = new TreeMap<>();
    private ArrayList<String> featureList;
    Analyzer analyzer = new StandardAnalyzer();
    private IndexWriter iwriter;
    private Instance firstInstance = null;
    private final Directory directory;
    private DirectoryReader ireader;
    private IndexSearcher isearcher;

    private int instancesCount = 0;

    public Dataset(String datasetName) throws IOException {
        directory = new RAMDirectory();

        initWriter();
        this.datasetName = datasetName;

    }

    private void initWriter() throws IOException {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        iwriter = new IndexWriter(directory, config);
    }

    public void addInstance(Instance instance){
        if(firstInstance == null) {
            firstInstance = instance;
            calcHeaderMapIfNeeded(instance);
        }
        instancesCount++;
        addInstanceToLucene(instance);
        updateClassCount(instance);
        addInstanceToClassInstanceMap(instance);

    }

    private void calcHeaderMapIfNeeded(Instance instance) {
        if(featureList == null) {
            setHeaderMap(extractHeaderMapFromInstances(instance));
            calcFeatureList();
        }
    }

    private static Map<String,Integer> extractHeaderMapFromInstances(Instance instance) {
        TreeMap<String,Integer> columns = new TreeMap<>();
        for(int i = 0; i < instance.size();i++){
            columns.put(String.valueOf(i),i);
        }
        return columns;
    }

    private void addInstanceToLucene(Instance instance) {
        Document doc = new Document();

        for (int i = 0; i < featureList.size(); i++) {
            doc.add(new Field(featureList.get(i),instance.getValue(i), TextField.TYPE_STORED));
        }
        try {
            iwriter.addDocument(doc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addInstanceToClassInstanceMap(Instance instance) {
        ArrayList<Instance> instancesPerClass = classInstancesMap.get(instance.getClassValue());
        if(instancesPerClass !=null){
            instancesPerClass.add(instance);
        }else{
            instancesPerClass = new ArrayList<>();
            instancesPerClass.add(instance);
            classInstancesMap.put(instance.getClassValue(),instancesPerClass);
        }
    }

    private void updateClassCount(Instance instance) {
        String classValue = instance.getClassValue();
        if(classesCount.containsKey(classValue)){
            Integer currentValue = classesCount.remove(classValue);
            classesCount.put(classValue,currentValue+1);
        }else{
            classesCount.put(classValue,1);
        }
    }

    public void setHeaderMap(Map<String,Integer> headerMap) {
        this.headerMap = headerMap;
        invertedHeaderMap = new HashMap<>();
        headerMap.forEach((s,i)->{
            invertedHeaderMap.put(i,s);
        });
    }

    public void setFeatureTypeMap() {
        if(firstInstance != null)
        {
            Instance instance = getFirstInstance();
            headerMap.forEach((s,i)->{
                String value = instance.getValue(i);
                if(value!=null)
                    featureTypeMap.put(s, TypeHelper.getType(value));
            });
        }
    }

    public void setClassFeatureIndex() {
        this.classFeatureIndex = headerMap.size()-1;
    }

    public ArrayList<String> getClassValues() {

        List<Map.Entry<String, Integer>> entries = CollectionUtils.entriesSortedByValues(classesCount, CollectionUtils.Order.DESC);

        ArrayList<String> list = new ArrayList<>();
        entries.forEach((entry)-> list.add(entry.getKey()));

        return list;
    }

    public ArrayList<Instance> getInstancesWithClass(String classValue) {
        return classInstancesMap.get(classValue);
    }

    public ArrayList<String> calcFeatureList() {
        if(featureList == null) {
            featureList = new ArrayList<>(invertedHeaderMap.values());
        }
        return featureList;
    }

    public Instance getFirstInstance() {
        return firstInstance;
    }

    public void finishedLoadingInstances() {
        try {
            iwriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int countInstancesWithFeatureValue(int featureIndex, String featureValue) throws IOException {
        initReaderAndSearcherIfNeeded();
        // Parse a simple query that searches for "text":
        QueryParser parser = new QueryParser(featureList.get(featureIndex), analyzer);
        Query query = null;

        try {
            //The addition of the " is useful to search for words without having to escape special characters with lucene
            query = new QueryParser(featureList.get(featureIndex),analyzer).parse("\"" + featureValue + "\"");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isearcher.count(query);

    }

    private void initReaderAndSearcherIfNeeded() throws IOException {
        if(ireader == null)
            ireader = DirectoryReader.open(directory);
        if(isearcher == null)
            isearcher = new IndexSearcher(ireader);
    }

    public ArrayList<Instance> queryInstances(RuleConjunction rule, boolean shouldRemove) throws IOException {
        initReaderAndSearcherIfNeeded();

        BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();

        if (rule.rulesSetSize() > 1){
            for (Rule r :
                    rule.getRules()) {
                try {
                    booleanQuery.add(new QueryParser(getFeatureName(r),analyzer).parse("\"" + r.getFeatureValue().toString() + "\""), BooleanClause.Occur.MUST);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }else{
            Rule r = rule.getRule();
            try {
                booleanQuery.add(new QueryParser(getFeatureName(r),analyzer).parse("\"" + r.getFeatureValue().toString() + "\""), BooleanClause.Occur.MUST);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        BooleanQuery query = booleanQuery.build();
        int count = isearcher.count(query);
        ArrayList<Instance> instances = new ArrayList<>();

        if(count == 0)
            return instances;
        ScoreDoc[] hits = isearcher.search(query, count).scoreDocs;
        for (int i = 0; i < hits.length; i++) {
            Document hitDoc = isearcher.doc(hits[i].doc);
            instances.add(extractInstanceFromDocument(hitDoc));
        }
        if(shouldRemove){
            ireader.close();
            ireader = null;
            isearcher = null;
            initWriter();
            iwriter.deleteDocuments(query);
            iwriter.close();
            initReaderAndSearcherIfNeeded();
            instancesCount-=hits.length;

        }
        return instances;
    }

    private String getFeatureName(Rule r) {
        return featureList.get(r.getFeatureIndex());
    }

    public Document randomDoc() throws IOException {
        int docNo = (int)(new java.util.Random().nextDouble() * ireader.maxDoc());
        Document document = ireader.document(docNo);

        ireader.close();
        ireader = null;
        isearcher = null;
        initWriter();
        ArrayList<Term> terms = new ArrayList<>();
        for (IndexableField field: document.getFields()) {
            terms.add(new Term(field.name(),document.get(field.name())));
        }

        iwriter.deleteDocuments(terms.toArray(new Term[terms.size()]));
        iwriter.close();
        initReaderAndSearcherIfNeeded();


        return document;
    }

    public ArrayList<Instance> queryInstances(Rule rule) throws IOException {
        initReaderAndSearcherIfNeeded();

        QueryParser parser = new QueryParser(getFeatureName(rule), analyzer);
        Query query = null;
        try {
            query = parser.parse(rule.getFeatureValue().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ScoreDoc[] hits = isearcher.search(query, isearcher.count(query)).scoreDocs;
        ArrayList<Instance> instances = new ArrayList<>();
        for (int i = 0; i < hits.length; i++) {
            Document hitDoc = isearcher.doc(hits[i].doc);
            instances.add(extractInstanceFromDocument(hitDoc));
        }
        return instances;
    }


    private Instance extractInstanceFromDocument(Document hitDoc) {
        Instance instance = new Instance();
        for (int i = 0; i < featureList.size(); i++) {
            instance.addValue(i,hitDoc.get(featureList.get(i)));
        }
        return instance;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public int countInstancesForClass(String classValue) {
        ArrayList<Instance> instances = classInstancesMap.get(classValue);
        if(instances == null)
            return 0;
        return instances.size();
    }

    public int getSize() {
        return instancesCount;
    }

    public Dataset extractTestDataset(int percentageOfOriginalDataset) throws IOException {

        Dataset d = new Dataset(datasetName + "_TEST");
        initReaderAndSearcherIfNeeded();
        for (int i = 0; i < getSize()/percentageOfOriginalDataset; i++) {
            Document hitDoc = randomDoc();
            d.addInstance(extractInstanceFromDocument(hitDoc));

        }
        return d;
    }
}

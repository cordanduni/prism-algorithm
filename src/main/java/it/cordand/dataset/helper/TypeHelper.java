package it.cordand.dataset.helper;

import java.lang.reflect.Type;

public class TypeHelper {

    public static Type getType(String value) {
        try{
            Integer.parseInt(value);
            return Integer.class;
        }catch (Exception ignored){}
        try{
            Double.parseDouble(value);
            return Double.class;
        }catch (Exception ignored){}

        return String.class;
    }
}

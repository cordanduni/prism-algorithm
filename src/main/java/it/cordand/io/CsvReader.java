package it.cordand.io;

import it.cordand.dataset.Dataset;
import it.cordand.dataset.Instance;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class CsvReader {

    public static Dataset[] readCSVContent(String filename, String datasetName, int percentageForTest) throws IOException {
        FileReader fileReader = null;

        Dataset dataset = new Dataset(datasetName);
        Dataset testDataset = new Dataset(datasetName+"_TEST");
        try {
            CSVParser csvFileParser = CSVFormat.DEFAULT.parse(new FileReader(new File(filename)));

            for (CSVRecord record:csvFileParser)
            {
                if(new Random().nextInt(100) > percentageForTest)
                    dataset.addInstance(new Instance(record));
                else
                    testDataset.addInstance(new Instance(record));
            }
            dataset.finishedLoadingInstances();
            testDataset.finishedLoadingInstances();

            dataset.setClassFeatureIndex();
            testDataset.setClassFeatureIndex();

            dataset.setFeatureTypeMap();
            testDataset.setFeatureTypeMap();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Dataset[]{dataset,testDataset};
    }
}

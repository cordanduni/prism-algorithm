package it.cordand;

import it.cordand.dataset.Dataset;
import it.cordand.io.CsvReader;
import it.cordand.ml.analyzer.PerformanceAnalysis;
import it.cordand.ml.analyzer.PerformanceAnalyzer;
import it.cordand.ml.model.RuleConjunction;
import it.cordand.ml.prism.Prism;
import it.cordand.storage.FileManager;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{

    public static final String DATASETS_LENSES_LENSES_DATA_CSV = "datasets/lenses/lenses.data.csv";
    public static final String DATASETS_POKER_LENSES_DATA_CSV = "datasets/poker/poker-hand-training-true.data.txt";
    public static final String DATASETS_POKER_TEST_LENSES_DATA_CSV = "datasets/poker/poker-hand-testing.data.txt";
    private static final String DATASETS_LENSES_LENSES_DATA_CSV_REDUCED = "datasets/lenses/lenses.data.reduced.csv";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() throws IOException {
//        Dataset dataset = CsvReader.readCSVContent("datasets/iris/iris.data.csv","iris");
        Dataset[] dataset = CsvReader.readCSVContent(DATASETS_LENSES_LENSES_DATA_CSV,"lenses",40);
//        Dataset[] trainingDataset = CsvReader.readCSVContent(DATASETS_POKER_TEST_LENSES_DATA_CSV,"poker",0);
        Prism prism = new Prism(dataset[0]);
        prism.generateRulesSet();
        PerformanceAnalyzer performanceAnalyzer = new PerformanceAnalyzer(prism);
        PerformanceAnalysis performanceAnalysis = performanceAnalyzer.testAgainstDataset(dataset[1]);
        FileManager.writeReportToFile(performanceAnalysis);

        String outputFile = FileManager.writeRulesToFile(prism);


    }
}
